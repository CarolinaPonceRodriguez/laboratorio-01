package ar.utn.dds.labo01;

public class OtraClase {
	
	//comentario agregado
	private String nombre;
	private int numero;
	
	
	public OtraClase(String nombre, int numero) {
		super();
		this.nombre = nombre;
		this.numero = numero;
	}
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
